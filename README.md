### What is this repository for? ###

This is poke at one of google's coding challanges for potential interview candidates
This is by no means the most efficient way to complete the challenge, it is just a solution that uses the standard <regex> libary.

The challenge is to write a function that takes a string and a group_size
and returns a formatted string that looks like a product key

The string is first stripped of all existing hyphens, and then a hyphen is
inserted each group_size any extra characters are ommited. For example:
string: "as-odify----efllja" group_size: 3 -> "aso-dif-yef-llj"


### How do I get set up? ###

To compile, use at least C++11. 
Google's challange permits everything up to C++14