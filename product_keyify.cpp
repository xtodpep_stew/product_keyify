#include <regex>
#include <algorithm>
#include <string>
#include <iterator>
#include <iostream>

std::string product_keyify(const std::string& str, unsigned group_size)
{
    std::regex group_re{".{" + std::to_string(group_size) + "}"};
    std::string no_dash = std::regex_replace(str, std::regex{"-"}, "");
    std::string result;
    std::for_each(
        std::sregex_iterator(std::begin(no_dash), std::end(no_dash), group_re),
        std::sregex_iterator(),
        [&](std::smatch m){ result += (m.str() + "-"); }
    );
    if(result.back() == '-');
        result.pop_back();
    return result;
}

int main()
{
    //usage example:
    std::string subject{"as-odify----efllja"};
    int group_size = 3;
    std::string result = product_keyify(subject, group_size);

    std::cout << "staring string: " << subject << "\n"; 
    std::cout << "result: " << result << "\n";
}